import ipaddress

def file_to_iplist(filename):
    my_networks = []

    for line in filename:
        line = line.strip()
        network = ipaddress.ip_network(line)
        my_networks.append(network)

    return(my_networks)

def find_parent_network(ip_network, parent_networks):
    for parent in parent_networks:
        try:
            if ip_network.subnet_of(parent):
                return(parent)
                break
            else:
                continue
        except TypeError as e:
            continue
        except Exception as e:
            print(e.with_traceback)
            print(e)
            quit()


def main():
    with open('testdata/test_nets.txt') as infile:
        mynet_file = infile.readlines()
    with open('testdata/test_ips.txt') as infile:
        ip_file = infile.readlines()

    my_networks = file_to_iplist(mynet_file)
    ip_list = file_to_iplist(ip_file)

    for this_net in ip_list:
        parent_net = find_parent_network(this_net, my_networks)

        if parent_net:
            if this_net.version is 4 and this_net.prefixlen is 32:
                print(f'{this_net.network_address} is in my network ({parent_net.with_prefixlen})')
            elif this_net.version is 6 and this_net.prefixlen is 128:
                print(f'{this_net.network_address} is in my network ({parent_net.with_prefixlen})')
            else:
                print(f'{this_net.with_prefixlen} is in my network ({parent_net.with_prefixlen})')

if __name__ == '__main__':
    main()
