# ipnetcompare

## Description
Use to compare a list of IP addresses against a list of known IP networks. Works with both IPv4 and IPv6.

If using a file for the list of known networks it must be in the form of one network per line. The function ```get_networks_list()``` returns a list.

## Requirements
Python 3.7+
